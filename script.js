
const addButton = document.getElementById('add');
const mediaButton = document.getElementById('calc-media');
var cont = 0
var sumNota = 0

function calculaMedia() {
  return parseInt((sumNota/cont)*100)/100;
}

function addText(newValue){
    document.querySelector("textarea").value += newValue;
}

/*fazer tramento de dados inseridos pelo usuario. nãp captar strings*/

function isNumberValid(nota){
    const number = parseFloat(nota)
    console.log(number)
    if(!number) return false
    if (number >= 0 && number <= 10) return true
    return false
}

addButton.onclick = function() {
    let nota = document.getElementById('nota').value;
    if (!nota){
        window.alert("Por favor, insira uma nota.");
    }
    else if(!isNumberValid(nota)){
        window.alert("A nota digitada é inválida, por favor, insira uma nota válida.")
    }
    else{
        cont++;
        nota = parseInt(nota*100)/100 // transforma qualquer nota em uma nota de até duas casas decimais
        addText(`A nota ${cont} foi ${nota}\n`);
        sumNota += parseFloat(nota);
        document.getElementById('nota').value="";
    }
}

mediaButton.onclick = function() {
    const media = calculaMedia();
    document.getElementById('media').text += media;
}


